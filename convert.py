import xml.etree.ElementTree as ET
import json

def convert_vulnerability_to_junit(vulnerability):
    severity = vulnerability["severity"].lower()
    
    # Only process medium, high, or critical vulnerabilities
    if severity not in ["medium", "high", "critical"]:
        return None

    testcase = ET.Element("testcase")
    message = vulnerability["message"]
    file = vulnerability["location"]["file"]
    start_line = str(vulnerability["location"]["start_line"])

    name = f"Issue: {message} - File: {file} - Severity: {severity.capitalize()} - Start Line: {start_line}"
    testcase.set("name", name)
    testcase.set("classname", file)
    testcase.set("file", file)

    failure = ET.SubElement(testcase, "failure")
    failure.set("type", severity)
    failure.set("message", vulnerability['description'].replace('\n', ' '))

    return testcase

def convert_gitlab_sast_to_junit(input_file, output_file):
    with open(input_file, 'r') as f:
        sast_report = json.load(f)

    vulnerabilities = sast_report["vulnerabilities"]
    testsuite = ET.Element("testsuite")

    for vulnerability in vulnerabilities:
        testcase = convert_vulnerability_to_junit(vulnerability)
        if testcase is not None:  # Only add the testcase if it is not None
            testsuite.append(testcase)

    tree = ET.ElementTree(testsuite)

    with open(output_file, 'wb') as f:
        tree.write(f, encoding='utf-8', xml_declaration=True)

if __name__ == "__main__":
    input_file = "gl-sast-report.json"
    output_file = "gl-code-quality-report.xml"

    convert_gitlab_sast_to_junit(input_file, output_file)
