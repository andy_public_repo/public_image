# Start from the python:slim base image
FROM python:slim

# Install git
RUN apt-get update && apt-get install -y git

# Create app directory
RUN mkdir -p /app

# Copy the Python scripts into the Docker image
COPY container_convert.py /app/
COPY convert.py /app/

# Make Python scripts executable
RUN chmod +x /app/container_convert.py
RUN chmod +x /app/convert.py
