import sys
import xml.etree.ElementTree as ET
import json

def convert_vulnerability_to_junit(vulnerability):
    testcase = ET.Element("testcase")
    testcase.set("name", f"Package: {vulnerability['location']['dependency']['package']['name']} - Severity: {vulnerability['severity']} - Solution: {vulnerability['solution']}")
    testcase.set("classname", vulnerability["location"]["image"])

    # Add a failure element for critical and high severity vulnerabilities
    if vulnerability["severity"].lower() in ["critical", "high"]:
        failure = ET.SubElement(testcase, "failure")
        if vulnerability["severity"].lower() == "critical":
            failure.set("type", "error")
        else:
            failure.set("type", "failure")
        failure.set("message", vulnerability["description"].replace('\n', ' '))
    
    return testcase



def convert_container_scan_to_junit(input_file, output_file):
    with open(input_file, 'r') as f:
        container_scan_report = json.load(f)

    vulnerabilities = container_scan_report["vulnerabilities"]
    testsuites = ET.Element("testsuites")
    testsuite = ET.Element("testsuite")
    
    for vulnerability in vulnerabilities:
        testcase = convert_vulnerability_to_junit(vulnerability)
        testsuite.append(testcase)
    
    testsuites.append(testsuite)
    tree = ET.ElementTree(testsuites)
    tree.write(output_file, encoding='utf-8', xml_declaration=True)


def check_for_critical_or_high_severity_vulnerabilities(input_file):
    with open(input_file, 'r') as f:
        container_scan_report = json.load(f)

    vulnerabilities = container_scan_report["vulnerabilities"]
    
    for vulnerability in vulnerabilities:
        if vulnerability["severity"].lower() in ["critical", "high"]:
            print("Found a critical or high severity vulnerability")
            sys.exit(1)


if __name__ == "__main__":
    input_file = "gl-container-scanning-report.json"
    output_file = "gl-container-scanning-report.xml"

    convert_container_scan_to_junit(input_file, output_file)
    check_for_critical_or_high_severity_vulnerabilities(input_file)
